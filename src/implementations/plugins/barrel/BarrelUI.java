package implementations.plugins.barrel;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import interfaces.plugins.UIPlugin;

public class BarrelUI implements UIPlugin {

	@Override
	public BufferedImage getTerrainImage(String terrainType) {
		BarrelRoll.log(terrainType);

		File imFile = null;

		// For game over
		if (terrainType.startsWith("gameover")) {
			imFile = new File("src/implementations/plugins/barrel/images/gameover",
					terrainType + ".jpg");

		} else if (terrainType.startsWith("background")) {
			imFile = new File("src/implementations/plugins/barrel/images/background",
					terrainType + ".jpg");
		} else {
			switch (BarrelRoll.TerrainType.valueOf(terrainType)) {
			case HOLE:
				imFile = new File("src/implementations.plugins.barrel.implementation/images/",
						"question.png");
				break;
			case BACKGROUND:
				BufferedImage im = new BufferedImage(10, 10,
						BufferedImage.TYPE_INT_RGB);
				Graphics2D g = im.createGraphics();
				g.setColor(Color.GRAY);
				g.fillRect(0, 0, 10, 10);
				return im;
			}
		}

		try {
			return ImageIO.read(imFile);
		} catch (IOException | IllegalArgumentException e) {
			// Dafault
			BufferedImage im = new BufferedImage(100, 100,
					BufferedImage.TYPE_INT_RGB);
			Graphics2D g = im.createGraphics();
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, 100, 100);
			g.setColor(Color.BLACK);
			g.fillOval(10, 10, 80, 80);
			return im;
		}
	}

	@Override
	public BufferedImage getPieceImage(String pieceType) {
		BarrelRoll.log(pieceType);

		try {
			return ImageIO.read(new File("src/implementations/plugins/barrel/images/",
					"question.png"));
		} catch (IOException e) {
			BufferedImage im = new BufferedImage(100, 100,
					BufferedImage.TYPE_INT_ARGB);
			Graphics2D g = im.createGraphics();
			g.setColor(Color.YELLOW);
			g.fillOval(0, 0, 100, 100);

			return im;
		}

	}

}
