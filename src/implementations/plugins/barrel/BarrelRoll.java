package implementations.plugins.barrel;

import implementations.model.ComputerImpl;
import implementations.model.PositionImpl;
import implementations.panels.InfoPanelImpl;
import implementations.panels.InstructionsPanelImpl;
import implementations.panels.elements.TextLabel;
import implementations.plugins.barrel.model.pieces.Bullet;
import implementations.plugins.barrel.model.players.Gun;
import interfaces.model.GameBoard;
import interfaces.model.GameState;
import interfaces.model.Piece;
import interfaces.model.Player;
import interfaces.model.Position;
import interfaces.model.SquareBoard;
import interfaces.plugins.GamePlugin;
import interfaces.view.panels.InfoPanel;
import interfaces.view.panels.InstructionsPanel;

import java.awt.Point;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class BarrelRoll implements GamePlugin {

	private int maxTeams;

	private int minTeams;

	private int maxPlayers;

	private int minPlayers;

	private int maxPerTeam;

	private int minPerTeam;

	private String[] computerTypes;
	private final static Random rand = new Random();
	private final static int randomKill = 50;

	private Player winner = null;
	private Player justDead = null;

	private Queue<Player> playerQueue = new LinkedList<Player>(); // ordering of
	// players
	private static Player Gun;
	private boolean runCleanUp;

	public final static String HOLE = "BulletHole";

	enum TerrainType {
		CYLINDER, HOLE, BACKGROUND;

		public static TerrainType getForSymbol(char symbol) {
			switch (symbol) {
			case 'o':
				return HOLE;
			case '.':
				return CYLINDER;
			default:
				return BACKGROUND;
			}
		}
	}

	enum PieceType {

	}

	private LinkedList<Bullet> bullets = new LinkedList<Bullet>();

	/**
	 * @param args
	 */
	public BarrelRoll() {
		log();
		setMaxTeams(6);
		setMinTeams(2);
		setMaxPlayers(6);
		setMinPlayers(2);
		setMaxPerTeam(1);
		setMinPerTeam(1);

		String[] types = { "Easy", "Medium", "Hard" };
		// String[] types = { "Medium"};
		setComputerTypes(types);
	}

	@Override
	public Position[][] setupBoard(Player[] players) {
		Gun = new Gun("gun", -1);
		for (Player p : players) {
			playerQueue.add(p);
		}

		String[] boardDef = { "       ", //
				"  o o  ", //
				"       ", //
				" o   o ", //
				"       ", //
				"  o o  ", //
				"       " };

		Position[][] board = new Position[boardDef.length][boardDef[0].length()];

		// Set terrain types
		for (int i = 0; i < boardDef.length; i++) {
			for (int j = 0; j < boardDef[i].length(); j++) {
				board[i][j] = new PositionImpl(null, new Point(i, j),
						"background_r" + (i + 1) + "_c" + (j + 1));

			}
		}

		int nbBullets = players.length - 1;
		// Set Piece types
		for (int i = 0; i < nbBullets; i++) {
			bullets.add(new Bullet(null, false));
		}
		for (int i = 0; i < 6 - nbBullets; i++) {
			bullets.add(new Bullet(null, true));
		}
		Collections.shuffle(bullets);
		rollCylinder(board);
		return board;
	}

	@Override
	public GameBoard runTurn(String commandInput, GameState currentState) {
		log();
		justDead = null;
		GameBoard currentBoard = currentState.getGameBoard();
		Player currentPlayer = playerQueue.peek();
		SquareBoard board;
		if (currentPlayer == null) {
			board = (SquareBoard) cleanUp(currentState);
			return board;
		} else {
			if (commandInput.equals("activate swag")) {
				// Kills everyone except the player
				playerQueue = new LinkedList<Player>();
				playerQueue.add(currentPlayer);
			} else if (commandInput.equals("can't touch this")) {
				// fake the shot
				playerQueue.poll();
				playerQueue.add(currentPlayer);
			} else if (commandInput.equals("suicide")) {
				playerQueue.poll();
			} else {
				try {
					int bullet_number = (Integer.parseInt(commandInput
							.substring(commandInput.lastIndexOf(' ') + 1))) % 6;
					System.out.println("The bullet number " + bullet_number);
					Bullet bullet = bullets.get(bullet_number);
					Player p = playerQueue.poll();
					if (bullet.getBlank()) {

						playerQueue.add(p);
					} else {
						justDead = p;
					}
					bullet.fire();

				} catch (NumberFormatException e) {
					return null;
				}
			}
			return currentBoard;
		}
	}

	@Override
	public Player whosTurn(GameState currentState) {
		// JavaDoc says that cleanUp happens only if this returns null
		// We want to cleanUp every 2 turns.
		log();
		if (runCleanUp) {
			runCleanUp = !(runCleanUp);
			System.out.println("runCleanUp");
			return null;
		}
		runCleanUp = !(runCleanUp);
		if (playerQueue.peek() instanceof ComputerImpl) {
			computerTurn(((ComputerImpl) (playerQueue.peek())).type(),
					currentState);
			return null;

		}
		System.out.println("Just before returning "
				+ playerQueue.peek().getName());
		return playerQueue.peek();
	}

	@Override
	public int[] gameOver(GameState currentState) {

		log();
		if (playerQueue.size() > 1) {
			return null;
		}
		finish(currentState);
		int[] tab = new int[1];
		if (winner != null) {
			tab[0] = winner.getTeamNumber();
			return tab;
		} else {
			winner = playerQueue.poll();
			return null;
		}
	}

	@Override
	public GameBoard cleanUp(GameState currentState) {
		rollCylinder(currentState.getGameBoard().getBoard());
		printState();
		return currentState.getGameBoard();
	}

	@Override
	public InfoPanel makePositionInfoPanel(Position position) {
		InfoPanel retPanel = new InfoPanelImpl();
		Piece p = position.getPiece();
		if (p != null) {
			retPanel.addElement(new TextLabel("Is it a bullet ? "));
		} else {
			retPanel.addElement(new TextLabel("Obviously no bullet here ! "));
		}

		return retPanel;
	}

	@Override
	public InfoPanel makeGameInfoPanel(GameState currentState) {
		log();
		InfoPanel retPanel = new InfoPanelImpl();
		if (winner == null) {
			if (justDead != null) {
				retPanel.addElement(new TextLabel("Oups : "
						+ justDead.getName() + " just died ! and "));
			}
			retPanel.addElement(new TextLabel("It's "
					+ playerQueue.peek().getName() + "'s turn to play!"));

		} else {
			retPanel.addElement(new TextLabel("Player " + winner.getName()
					+ " won the game !"));
		}
		return retPanel;
	}

	@Override
	public InstructionsPanel makeInstructionsPanel(GameState currentState) {
		log();

		return new InstructionsPanelImpl("Enter a bullet number");

	}

	private Position[][] initBoard(Position[][] board) {
		// TODO Auto-generated method stub
		return board;
	}

	@Override
	public int getMaxTeams() {
		return maxTeams;
	}

	@Override
	public int getMinTeams() {
		return minTeams;
	}

	@Override
	public int getMaxPlayers() {
		return maxPlayers;
	}

	@Override
	public int getMinPlayers() {
		return minPlayers;
	}

	@Override
	public int getMaxPerTeam() {
		return maxPerTeam;
	}

	@Override
	public int getMinPerTeam() {
		return minPerTeam;
	}

	@Override
	public String[] computerTypes() {
		return computerTypes;
	}

	public void setMaxTeams(int maxTeams) {
		this.maxTeams = maxTeams;

	}

	public void setMinTeams(int minTeams) {
		this.minTeams = minTeams;

	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;

	}

	public void setMinPlayers(int minPlayers) {
		this.minPlayers = minPlayers;

	}

	public void setMaxPerTeam(int maxPerTeam) {
		this.maxPerTeam = maxPerTeam;

	}

	public void setMinPerTeam(int minPerTeam) {
		this.minPerTeam = minPerTeam;

	}

	public void setComputerTypes(String[] computerTypes) {
		this.computerTypes = computerTypes;
	}

	private void shuffle(int order) {
		for (int i = 0; i <= order; i++) {
			Bullet b = bullets.poll();
			bullets.addLast(b);
		}
	}

	private void rollCylinder(Position[][] board) {
		log();
		Random rand = new Random();
		// we make the cylinder turn
		shuffle(rand.nextInt(5));
		if (winner == null) {
			board[1][2].setPiece(bullets.get(0));
			board[1][4].setPiece(bullets.get(1));
			board[3][1].setPiece(bullets.get(2));
			board[3][5].setPiece(bullets.get(3));
			board[5][2].setPiece(bullets.get(4));
			board[5][4].setPiece(bullets.get(5));
		}
	}

	private void finish(GameState gameState) {

		GameBoard board = gameState.getGameBoard();
		for (int i = 0; i < board.getHeight(); i++) {
			for (int j = 0; j < board.getWidth(); j++) {
				Position pos = board.getPositionAt(new Point(i, j));
				pos.setTerrainType("gameover_r" + (i + 1) + "_c" + (j + 1));
				pos.setPiece(null);
			}
		}
	}

	private void computerTurn(String type, GameState current) {
		int rand1 = rand.nextInt(100) % (bullets.size());
		int rand2 = rand.nextInt(100) % (bullets.size());
		switch (type) {
		case "Medium":
			runTurn(rand.nextInt(100) + "", current);
			break;
		case "Hard":

			if (!bullets.get(rand1).getBlank()
					&& !bullets.get(rand2).getBlank()) {
				bullets.get(rand1).fire();
				runTurn("suicide", current);
			} else {
				runTurn("can't touch this", current);
			}
			break;
		default:
			if (!bullets.get(rand1).getBlank()) {
				bullets.get(rand1).fire();
				runTurn("suicide", current);
			} else if (!bullets.get(rand2).getBlank()) {
				bullets.get(rand2).fire();
				runTurn("suicide", current);
			} else {
				runTurn("can't touch this", current);
			}
			break;

		}

	}

	static void log(String... a) {
		StackTraceElement elem = Thread.currentThread().getStackTrace()[2];
		if (false) {
			StringBuilder sb = new StringBuilder("[Log]");
			sb.append("[" + elem.getClassName() + "]");
			sb.append("[" + elem.getMethodName() + "] ");
			for (String s : a) {
				sb.append(s + " ");
			}
			System.out.println(sb.toString());
		}
	}

	private void printState() {
		if (true) {
			int i = 0;
			for (Bullet bullet : bullets) {

				System.out.println("Bullet number " + i + " blank :"
						+ bullet.getBlank());
				i++;
			}
		}
	}

}
