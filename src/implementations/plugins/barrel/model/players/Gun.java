package implementations.plugins.barrel.model.players;

import implementations.model.PlayerImpl;
import interfaces.model.Piece;

import java.util.List;

/**
 * This class is used to keep the pieces. In fact all the pieces need to have a
 * player when they are created, we can fire them.
 * 
 * @author aghosn
 * 
 */
public class Gun extends PlayerImpl {

    private List<Piece> pieces;

    /**
     * Set team to "Gun" Set team to -1 by default -1 should be reserved to the
     * Gun and no other player !
     * 
     * @param name
     * @param team
     */
    public Gun(String name, int team) {
	super("Gun", -1);
	// TODO Auto-generated constructor stub
    }

    public List<Piece> getPieces() {
	return pieces;
    }

    public void addPiece(Piece piece) {
	pieces.add(piece);
    }

    public void removePiece(Piece piece) {
	pieces.remove(piece);
    }

}
