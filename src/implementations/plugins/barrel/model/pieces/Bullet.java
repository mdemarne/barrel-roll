package implementations.plugins.barrel.model.pieces;

import implementations.model.PieceImpl;
import interfaces.model.Player;

public class Bullet
    extends PieceImpl
{

    // false if the bullet can kill
    // true if the bullet cannot
    private boolean blank;

    public Bullet(Player player)
    {
        super(player);
        this.blank = true;

    }

    /**
     * Powder is the value for the blank attribute True = bullet can't kill
     * False = bullet can kill
     * 
     * @param player
     * @param powder
     */
    public Bullet(Player player, boolean blank)
    {
        this(player);
        setPieceType(blank?"blank":"full");
        this.blank = blank;
    }

    public boolean getBlank()
    {
        return this.blank;
    }

    public void setBlank(boolean b)
    {
        this.blank = b;
    }

    public void fire()
    {
        this.blank = true;
    }

    public void reload()
    {
        this.blank = false;
    }

}
