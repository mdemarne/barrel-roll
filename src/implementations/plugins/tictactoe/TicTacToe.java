package implementations.plugins.tictactoe;

import implementations.model.PieceImpl;
import implementations.model.PositionImpl;
import implementations.panels.InfoPanelImpl;
import implementations.panels.InstructionsPanelImpl;
import implementations.panels.elements.TextLabel;
import interfaces.model.GameBoard;
import interfaces.model.GameState;
import interfaces.model.Piece;
import interfaces.model.Player;
import interfaces.model.Position;
import interfaces.plugins.GamePlugin;
import interfaces.view.panels.InfoPanel;
import interfaces.view.panels.InstructionsPanel;

import java.awt.Point;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TicTacToe implements GamePlugin {

    private int maxTeams;
    private int minTeams;
    private int maxPlayers;
    private int minPlayers;
    private int maxPerTeam;
    private int minPerTeam;
    private String[] computerTypes;

    private Player player1;
    private Player player2;
    private int turn = 0;

    private Player winner = null;

    public static final String GRID = "grid";

    private final String[] pieces = {//

    "       ",//
	    "x     o",//
	    "x     o",//
	    "x     o",//
	    "x     o",//
	    "x     o",//
	    "       ",//
    };

    public enum PieceType {
	X, O;
    }

    private Player getPlayer(PieceType type) {
	switch (type) {
	case X:
	    return player1;
	case O:
	    return player2;
	}
	return null;
    }

    public TicTacToe() {
	setMinPlayers(2);
	setMaxPlayers(2);
	setMinTeams(2);
	setMaxTeams(2);
	setMinPerTeam(1);
	setMaxPerTeam(1);
	setComputerTypes(new String[0]);
    }

    @Override
    public InfoPanel makePositionInfoPanel(Position position) {
	InfoPanel rv = new InfoPanelImpl();
	if (position.getPiece() == null) {
	    rv.addElement(new TextLabel("No Piece"));
	} else {
	    rv.addElement(new TextLabel("Piece of player "
		    + position.getPiece().getPlayer().getName()));
	}
	return rv;
    }

    @Override
    public InfoPanel makeGameInfoPanel(GameState currentState) {
	InfoPanel rv = new InfoPanelImpl();
	if (winner == null) {
	    if (turn >= 9) {
		rv.addElement(new TextLabel("It is a tie!"));
	    } else {
		rv.addElement(new TextLabel("It is "
			+ whosTurn(currentState).getName() + "'s turn to play."));
	    }
	} else {

	    rv.addElement(new TextLabel("Winner : " + winner.getName()));
	}
	return rv;
    }

    @Override
    public InstructionsPanel makeInstructionsPanel(GameState currentState) {
	return new InstructionsPanelImpl("Enter a \nposition to \nplay as i-j, \nwhere i is the \nline number \nand j the \ncolumn \nline number");
    }

    @Override
    public Position[][] setupBoard(Player[] players) {
	Position[][] board = new Position[7][7];
	player1 = players[0];
	player2 = players[1];
	for (int i = 0; i < 7; i++) {
	    for (int j = 0; j < 7; j++) {
		char pChar = pieces[i].charAt(j);
		String terrainType = "background_r" + (i + 1) + "_c" + (j + 1);
		Point p = new Point(i, j);
		if (pChar != ' ') {
		    PieceType pType = PieceType.valueOf(("" + pChar)
			    .toUpperCase());
		    board[i][j] = new PositionImpl(new PieceImpl(
			    getPlayer(pType), pType.toString()), p, terrainType);
		} else {
		    board[i][j] = new PositionImpl(null, p, terrainType);
		}
	    }
	}
	return board;
    }

    public void finish(GameState currentState) {
	GameBoard board = currentState.getGameBoard();
	for (int i = 0; i < board.getHeight(); i++) {
	    for (int j = 0; j < board.getHeight(); j++) {
		Position p = board.getPositionAt(new Point(i, j));
		p.setPiece(null);
		p.setTerrainType("gamefinished_r" + (i + 1) + "_c" + (j + 1));
	    }
	}
    }

    @Override
    public GameBoard runTurn(String commandInput, GameState currentState) {
	Pattern pat = Pattern.compile("([1-3])-([1-3])");
	Matcher mat = pat.matcher(commandInput);
	if (mat.matches()) {
	    // printBoard(currentState.getGameBoard());
	    int i = Integer.parseInt(mat.group(1));
	    int j = Integer.parseInt(mat.group(2));
	    GameBoard board = currentState.getGameBoard();
	    Position start = board.getPositionAt(new Point(turn / 2 + 1,
		    (turn % 2) * 6));
	    Position end = board.getPositionAt(new Point(i + 1, j + 1));
	    if (end.getPiece() != null) {
		return null;
	    } else {
		end.setPiece(start.getPiece());
		start.setPiece(null);
		turn++;
		return board;
	    }
	} else {
	    System.out.println("Invalid Input");
	    return null;
	}
    }

    @Override
    public Player whosTurn(GameState currentState) {
	return winner != null ? null : turn % 2 == 0 ? player1 : player2;
    }

    @Override
    public int[] gameOver(GameState currentState) {
	char[][] board = new char[3][3];
	for (int i = 2; i < 5; i++) {
	    for (int j = 2; j < 5; j++) {
		Piece p = currentState.getGameBoard()
			.getPositionAt(new Point(i, j)).getPiece();
		if (p != null) {
		    board[i - 2][j - 2] = p.getPieceType().charAt(0);
		} else {
		    board[i - 2][j - 2] = '.';
		}
	    }
	}

	if (winner != null) {
	    int[] winnerTeam = new int[1];
	    winnerTeam[0] = winner.getTeamNumber();
	    return winnerTeam;
	} else if (turn >= 9) {
	    return new int[0];
	} else {
	    char win = checkWin(board);
	    if (win != '.') {
		this.winner = getPlayer(PieceType.valueOf(win + ""));
		finish(currentState);
	    } else {
		if (turn == 8) {
		    turn++;
		    finish(currentState);
		}
	    }
	    return null;
	}
    }

    private char checkWin(char[][] board) {
	char a = checkLines(board);
	char b = checkCols(board);
	char c = checkDiag(board);
	if (a != '.') {
	    return a;
	} else if (b != '.') {
	    return b;
	} else {
	    return c;
	}
    }

    private char checkLines(char[][] board) {
	for (int i = 0; i < 3; i++) {
	    char c = board[i][0];
	    for (int j = 0; j < 3; j++) {
		if (c != board[i][j]) {
		    c = '.';
		}
	    }
	    if (c != '.') {
		return c;
	    }
	}
	return '.';
    }

    private char checkDiag(char[][] board) {
	if (board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
	    return board[0][0];
	} else if (board[0][2] == board[1][1] && board[2][0] == board[1][1]) {
	    return board[1][1];
	}
	return '.';
    }

    private char checkCols(char[][] board) {
	for (int i = 0; i < 3; i++) {
	    char c = board[0][i];
	    for (int j = 0; j < 3; j++) {
		if (c != board[j][i]) {
		    c = '.';
		}
	    }
	    if (c != '.') {
		return c;
	    }
	}
	return '.';
    }

    @Override
    public GameBoard cleanUp(GameState currentState) {
	return null;
    }

    public int getMaxTeams() {
	return maxTeams;
    }

    public void setMaxTeams(int maxTeams) {
	this.maxTeams = maxTeams;
    }

    public int getMinTeams() {
	return minTeams;
    }

    public void setMinTeams(int minTeams) {
	this.minTeams = minTeams;
    }

    public int getMaxPlayers() {
	return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
	this.maxPlayers = maxPlayers;
    }

    public int getMinPlayers() {
	return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
	this.minPlayers = minPlayers;
    }

    public int getMaxPerTeam() {
	return maxPerTeam;
    }

    public void setMaxPerTeam(int maxPerTeam) {
	this.maxPerTeam = maxPerTeam;
    }

    public int getMinPerTeam() {
	return minPerTeam;
    }

    public void setMinPerTeam(int minPerTeam) {
	this.minPerTeam = minPerTeam;
    }

    public String[] computerTypes() {
	return computerTypes;
    }

    public void setComputerTypes(String[] computerTypes) {
	this.computerTypes = computerTypes;
    }

    // private void printBoard(GameBoard board) {
    // System.out.println("Board State : ");
    // for (int i = 0; i < board.getHeight(); i++) {
    // String s = "";
    // for (int j = 0; j < board.getHeight(); j++) {
    // Piece p = board.getPositionAt(new Point(i, j)).getPiece();
    // if (p != null) {
    // s += p.getPieceType();
    // } else {
    // s += ".";
    // }
    // }
    // System.out.println(s);
    // }
    // System.out.println();
    // }
}
