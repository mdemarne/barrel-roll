package implementations.plugins.tictactoe;

import interfaces.plugins.UIPlugin;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class TicTacToeUI implements UIPlugin {

    @Override
    public BufferedImage getTerrainImage(String terrainType) {
	try {
	    return ImageIO.read(new File(
		    "src/implementations/plugins/tictactoe/images", terrainType
			    + ".jpg"));
	} catch (IOException e) {
	    BufferedImage im = new BufferedImage(100, 100,
		    BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g = im.createGraphics();

	    switch (terrainType) {
	    case TicTacToe.GRID:
		g.setColor(Color.WHITE);
		break;
	    default:
		g.setColor(Color.GRAY.darker().darker());
		break;
	    }

	    g.fillRect(0, 0, 100, 100);
	    return im;
	}
    }

    @Override
    public BufferedImage getPieceImage(String pieceType) {
	try {
	    return ImageIO.read(new File(
		    "src/implementations/plugins/tictactoe/images", pieceType
			    .toString() + ".png"));
	} catch (IOException e) {
	    BufferedImage im = new BufferedImage(100, 100,
		    BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g = im.createGraphics();
	    g.setColor(Color.BLACK);

	    switch (TicTacToe.PieceType.valueOf(pieceType)) {
	    case O:
		g.drawOval(0, 0, 100, 100);
		g.drawOval(1, 1, 99, 99);
		g.drawOval(2, 2, 98, 98);
		break;
	    case X:
		g.drawLine(0, 0, 100, 100);
		g.drawLine(0, 1, 99, 100);
		g.drawLine(1, 0, 100, 99);
		g.drawLine(0, 2, 98, 100);
		g.drawLine(2, 0, 100, 98);
		g.drawLine(0, 100, 100, 0);
		g.drawLine(0, 99, 99, 0);
		g.drawLine(1, 100, 100, 1);
		g.drawLine(0, 98, 98, 0);
		g.drawLine(2, 100, 100, 2);
		break;
	    }
	    return im;
	}
    }
}
