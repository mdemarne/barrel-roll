package implementations;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import interfaces.model.*;
import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;
import interfaces.view.GameFrame;
import interfaces.view.panels.*;

import implementations.panels.*;
import implementations.model.*;

public class GameFrameImpl extends JFrame implements GameFrame, ActionListener, MouseListener, Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1811448640357509689L;
	private BoardPanel boardPanel;
	private InfoHolderPanel infoPanel;
	private InputPanel inputPanel;
	private InstructionsPanel instructionsPanel;
	
	private Player[] players;
	private GamePlugin gamePlugin;
	private UIPlugin uiPlugin;
	
	private Player currentPlayer;
	
	private GameState gameState;
	
	String commandString = "";
	Object commandLock = new Object();
	
	public GameFrameImpl(String label, Player[] players, GamePlugin gamePlugin, UIPlugin uiPlugin){
		super(label);
		
		//Set the instance variables
		this.players = players;
		this.gamePlugin = gamePlugin;
		this.uiPlugin = uiPlugin;
		
		//Initialize the game
		Position[][] positions = gamePlugin.setupBoard(players);
		GameBoard gameBoard = new SquareBoardImpl(positions, positions[0].length, positions.length);
		this.gameState = new GameStateImpl(gameBoard, players);
		
		//Initial creation of panels
		boardPanel = new BoardPanelImpl(this, gameState, uiPlugin);
		infoPanel = new InfoHolderPanel(new InfoPanelImpl(), gamePlugin.makeGameInfoPanel(gameState));
		inputPanel = new InputPanelImpl(this);
		instructionsPanel = gamePlugin.makeInstructionsPanel(gameState);

		//Add all the panels
		getContentPane().add((InfoHolderPanel) infoPanel, BorderLayout.PAGE_START);
		getContentPane().add((BoardPanelImpl) boardPanel, BorderLayout.CENTER);
		getContentPane().add((InstructionsPanelImpl) instructionsPanel, BorderLayout.LINE_END);
		getContentPane().add((InputPanelImpl) inputPanel, BorderLayout.PAGE_END);
		
		//updatePanels();
		
		//Pack
		pack();
	}
	
	private void updatePanels(){
		infoPanel.update(gamePlugin, gameState);
		boardPanel.update(gameState);
		validate();
	}
	
	@Override
	public void run() {
		System.out.println("I've just started running");
		int[] gameOverArray;
		while((gameOverArray = gamePlugin.gameOver(gameState)) == null)
		{
			System.out.println("I've just started the while loop");
			currentPlayer = gamePlugin.whosTurn(gameState);
			
			GameBoard nextBoard = null;
			if(null == currentPlayer)
				nextBoard = gamePlugin.cleanUp(gameState);
			else{
				synchronized(commandLock){
					if(commandString.equals("") || commandString == null){
						try {
							System.out.println("I'm about to wait");
							commandLock.wait();
							System.out.println("I'm done waiting");
						} catch (InterruptedException e) {
							System.out.println("Caught unexpected interruption");
						}
						nextBoard = gamePlugin.runTurn(commandString, gameState);
						commandString = "";
					}
				}
			}
			if(nextBoard != null)
				gameState = new GameStateImpl(nextBoard,Arrays.asList(players));

			updatePanels();
		}
		
		if (gameOverArray.length == 0) // We have a tie - nobody won
		{
			JOptionPane.showMessageDialog(this, "Nobody won!", "Game Over", JOptionPane.PLAIN_MESSAGE);
			this.dispose();
			return;
		}
		else // somebody one.
		{
			String teamWinners = "";
			
			if (gameOverArray.length == 1)
			{
				teamWinners = "Team " + gameOverArray[0] + " won!";
			}
			else if (gameOverArray.length == 2)
			{
				teamWinners = "Teams " + gameOverArray[0] + " and "+ gameOverArray[1] + " won!";
			}
			else
			{
				teamWinners = "Teams ";
				for (int i = 0; i < gameOverArray.length; i++)
				{
					if (i == gameOverArray.length - 2)
						teamWinners += "and " + i + " won!";
					else
						teamWinners += i + ", ";
				}
			}
			
			ArrayList<String> names = new ArrayList<String>();
			
			for (Player player : players)
			{
				for (int winner : gameOverArray)
				{
					if (player.getTeamNumber() == winner)
					{
						names.add(player.getName());
					}
				}
			}
			
			String playerWinners = "";
			if (names.size() == 1)
			{
				playerWinners = "Great job " + names.get(0) + "!";
			}
			else if (names.size() == 2)
			{
				playerWinners = "Great job " + names.get(0) + " and " + names.get(1) + "!";
			}
			else
			{
				playerWinners = "Great job ";
				for (int i = 0; i < names.size(); i++)
				{
					if (i == names.size() - 2)
						playerWinners += "and " + names.get(i) + "!";
					else
						teamWinners += names.get(i) + ", ";
				}
			}
			teamWinners += "\n\n";
			JOptionPane.showMessageDialog(this, teamWinners + playerWinners, "Game Over", JOptionPane.PLAIN_MESSAGE);
			this.dispose();
		}
		
		
	}

    public static void createAndShowGUI(Player[] players, GamePlugin gamePlugin, UIPlugin uiPlugin, String gamePluginName) {
        //Create and set up the window.
        JFrame frame = new GameFrameImpl(gamePluginName, players, gamePlugin, uiPlugin);
 
        //Display the window.
        frame.setLocationRelativeTo(null);
        
        frame.setVisible(true);
        
        new Thread(((GameFrameImpl)frame)).start();
    }

	@Override
	public void actionPerformed(ActionEvent arg0) {
		synchronized(commandLock){
			commandString = inputPanel.getMoveText();
			commandLock.notifyAll();
		}
	}

	public void mouseClicked(MouseEvent event) {
		infoPanel.update(gamePlugin, ((Position)event.getComponent()));
		infoPanel.revalidate();
		validate();
	}

	@Override
	public void mouseEntered(MouseEvent event) {
		((PositionImpl)event.getComponent()).setBorder(BorderFactory.createLineBorder(Color.cyan));
	}

	@Override
	public void mouseExited(MouseEvent event) {
		((PositionImpl)event.getComponent()).setBorder(BorderFactory.createLineBorder(Color.black));
	}

	@Override
	public void mousePressed(MouseEvent arg0) {/*ignore*/}

	@Override
	public void mouseReleased(MouseEvent arg0) {/*ignore*/}
	
}
