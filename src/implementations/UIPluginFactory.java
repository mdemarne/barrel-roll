package implementations;

import interfaces.plugins.UIPlugin;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Locale;
import java.util.ResourceBundle;

public class UIPluginFactory {
	
	private static final String UI_PLUGINS_CONFIGURATION = "uiplugins";
	public static Hashtable<String, String> uIPluginMappings = new Hashtable<String, String>();

	static {
		try {
			loadUIPluginMappings();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static UIPlugin getUIPlugin(String pluginName) {
        String className = uIPluginMappings.get(pluginName);
 
        UIPlugin uIPlugin = null;
 
        try {
            if( className!=null) {
                Class<?> cls = Class.forName(className);
                uIPlugin = (UIPlugin)cls.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
 
        return uIPlugin;
    }
	
    private static void loadUIPluginMappings() {
        ResourceBundle rb = ResourceBundle.getBundle(UI_PLUGINS_CONFIGURATION, Locale.getDefault());
        for (Enumeration<String> e = rb.getKeys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            uIPluginMappings.put(key, rb.getString(key));
        }
    }

}
