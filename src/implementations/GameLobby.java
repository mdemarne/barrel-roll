package implementations;
 
import implementations.model.ComputerImpl;
import implementations.model.HumanImpl;
import implementations.plugins.Monopoly;
import interfaces.model.Player;
import interfaces.plugins.GamePlugin;
import interfaces.plugins.UIPlugin;
import interfaces.view.GameFrame;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
 
/** 
 * TableRenderDemo is just like TableDemo, except that it
 * explicitly initializes column sizes and it uses a combo box
 * as an editor for the Sport column.
 */
public class GameLobby extends JPanel {
    private boolean DEBUG = true;
    public static JFrame topFrame; 
    Object[][] data;
    JComboBox comboBox;
    JTable table;
    public GameLobby(final GamePlugin gamePlugin, final UIPlugin uiPlugin, final String gamePluginName) {
        super(new BorderLayout());
        
        
        data = new Object[gamePlugin.getMaxPlayers()][1];
        for (int i = 0 ; i < gamePlugin.getMaxPlayers() ; i++)
        {
        	String computerType;
        	if(gamePlugin.computerTypes() == null || gamePlugin.computerTypes().length == 0)
        		computerType = "";
        	else
        		computerType = gamePlugin.computerTypes()[0];

        	Object[] cols = {i + 1, "", "Human", 1, computerType};
        	data[i] = cols;
        }
        
        table = new JTable(new GameTableModel(data));
        table.setGridColor(Color.black);
        table.getTableHeader().setReorderingAllowed(false);
        
        table.setPreferredScrollableViewportSize(new Dimension(400, 200));
        table.setFillsViewportHeight(true);
        
        
        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);
 
        //Set up column sizes.
        initColumnSizes(table);
 
        // Setup comboboxes.
        setUpHumanComputerColumn(table, table.getColumnModel().getColumn(2), gamePlugin.computerTypes());
        setUpTeamColumn(table, table.getColumnModel().getColumn(3), gamePlugin.getMaxTeams());
        setUpComputerTypesColumn(table, table.getColumnModel().getColumn(4), gamePlugin.computerTypes());
 
        table.getColumnModel().getColumn(0).setMaxWidth(100);
        table.getColumnModel().getColumn(2).setMaxWidth(100);
        table.getColumnModel().getColumn(3).setMaxWidth(100);
        table.getColumnModel().getColumn(4).setMaxWidth(100);

        //Add the scroll pane to this panel.
        add(scrollPane, BorderLayout.PAGE_START);
        JButton okButton = new JButton("OK");
//        okButton.setSize(100, 20);
		
        //Get a reference to this so that ActionListener can add a Dialog to this.
		final GameLobby thisFrame = this;
        okButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (legalPlayers(data, gamePlugin))
				{
					System.out.println("Entering GameFrame...");
					
					//Close the lobby's frame
					topFrame.dispose();
					
			        javax.swing.SwingUtilities.invokeLater(new Runnable() {
			            public void run() {
			                GameFrameImpl.createAndShowGUI(getPlayers(data), gamePlugin, uiPlugin, gamePluginName);
			            }
			        });
				}
				else
				{
					String warning = "You have an incorrect setup for this game plugin.\n\nRequirements:\n\n";

					String minTeamsString = "Min teams: " + gamePlugin.getMinTeams() + "\n";
					String minPlayersString = "Min players: " + gamePlugin.getMinPlayers() + "\n";
					String maxPerTeamString = "Max players per team: " + gamePlugin.getMaxPerTeam() + "\n";
					String minPerTeamString = "Min players per team: " + gamePlugin.getMinPerTeam() + "\n";
					
					String requirements = minTeamsString + minPlayersString + maxPerTeamString + minPerTeamString;
					String again = "\nPlease try setting the game up again.";
					JOptionPane.showMessageDialog(thisFrame, warning + requirements + again);
				}
			}
		});
        add(okButton, BorderLayout.PAGE_END);
    }
    
    private Player[] getPlayers(Object[][] data) {
		
    	List<Player> playersList = new ArrayList<Player>();
    	for(Object[] row : data)
    	{

    		String name = (String) row[1];
    		String computerType = (String) row[4];
    		String playerType = (String) row[2];
    		int teamNumber = (Integer) row[3];
	
    		if (!name.equals("")) // skip rows with no names (they aren't players)
    		{
    			Player player;
    			if (playerType.equals("Human"))
    			{
    				player = new HumanImpl(name, teamNumber);
    			}
    			else // must be "Computer"
    			{
    				player = new ComputerImpl(name, teamNumber, computerType);
    			}
    			playersList.add(player);
    		}
    	}

    	Player[] players = playersList.toArray(new Player[playersList.size()]);
    	System.out.println(players);
    	return players;
	}
    
    private boolean legalPlayers(Object[][] data, GamePlugin gamePlugin)
    {    	
    	
    	// Check for minTeams
    	List<Integer> teamList = new ArrayList<Integer>();
    	for (Object[] row : data)
    	{
    		int teamNumber = (Integer) row[3];
    		teamList.add(teamNumber);
    	}
    	Set<Integer> teamSet = new HashSet<Integer>(teamList);
    	if (teamSet.size() < gamePlugin.getMinTeams())
    	{
    		return false;
    	}
    	
    	// Check for minPlayers
    	int playerCount = 0;
    	for (Object[] row : data)
    	{
    		if (!row[1].equals(""))
    		{
    			playerCount++;
    		}
    	}
    	if (playerCount < gamePlugin.getMinPlayers())
    	{
    		return false;
    	}
    	
    	// Check for maxPerTeam and minPerTeam
//    	System.out.println(teamSet);
    	for (int teamNumber : teamSet)
    	{
    		int playerForTeamCount = 0;
    		for (Object[] row : data)
    		{
    			if (row[3].equals(teamNumber) && !row[1].equals(""))
    			{
    				playerForTeamCount++;
    			}
    		}
    		if (playerForTeamCount > gamePlugin.getMaxPerTeam() || 
    				playerForTeamCount < gamePlugin.getMinPerTeam())
    			return false;
    	}
    	return true;
    }
 
    /*
     * This method picks good column sizes.
     * If all column heads are wider than the column's cells'
     * contents, then you can just use column.sizeWidthToFit().
     */
    private void initColumnSizes(JTable table) {
        GameTableModel model = (GameTableModel)table.getModel();
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        Object[] longValues = model.longValues;
        TableCellRenderer headerRenderer =
            table.getTableHeader().getDefaultRenderer();
 
        for (int i = 0; i < 5; i++) {
            column = table.getColumnModel().getColumn(i);
 
            comp = headerRenderer.getTableCellRendererComponent(
                                 null, column.getHeaderValue(),
                                 false, false, 0, 0);
            headerWidth = comp.getPreferredSize().width;
//        	System.out.print("i is :::: ");
//        	System.out.println(i);

            comp = table.getDefaultRenderer(model.getColumnClass(i)).
                             getTableCellRendererComponent(
                                 table, longValues[i],
                                 false, false, 0, i);
            cellWidth = comp.getPreferredSize().width;
 
            if (DEBUG) {
                System.out.println("Initializing width of column "
                                   + i + ". "
                                   + "headerWidth = " + headerWidth
                                   + "; cellWidth = " + cellWidth);
            }
 
            column.setPreferredWidth(Math.max(headerWidth, cellWidth));
        }
    }
 

    public void setUpComputerTypesColumn(JTable table,
            TableColumn teamColumn, String[] computerTypes) {
		//Set up the editor for the sport cells.
		comboBox = new JComboBox();
//		comboBox.setEnabled(false);
		if (computerTypes != null)
		{
			for (String type : computerTypes)
			{
				comboBox.addItem(type);
			}
		}
//		comboBox.addItem("Human");
//		comboBox.addItem("Computer");
		teamColumn.setCellEditor(new DefaultCellEditor(comboBox));
		
		DefaultTableCellRenderer renderer =
		new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for combo box");
		teamColumn.setCellRenderer(renderer);
		
    }

    public void setUpTeamColumn(JTable table,
            TableColumn teamColumn, int maxTeams) {
		//Set up the editor for the sport cells.
		JComboBox comboBox = new JComboBox();
		
		for (int i = 0; i < maxTeams ; i++)
		{
			comboBox.addItem(i + 1);

		}
		teamColumn.setCellEditor(new DefaultCellEditor(comboBox));
		
		DefaultTableCellRenderer renderer =
		new DefaultTableCellRenderer();
		renderer.setToolTipText("Click for combo box");
		teamColumn.setCellRenderer(renderer);
		
    }
    public void setUpHumanComputerColumn(JTable table,
                                 TableColumn humanComputerColumn,
                                 String[] computerTypes) {
        //Set up the editor for the sport cells.
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("Human");
        if (computerTypes != null && computerTypes.length != 0)
        	comboBox.addItem("Computer");
        
        humanComputerColumn.setCellEditor(new DefaultCellEditor(comboBox));
 
        DefaultTableCellRenderer renderer =
                new DefaultTableCellRenderer();
        renderer.setToolTipText("Click for combo box");
        humanComputerColumn.setCellRenderer(renderer);
    }
 
    class GameTableModel extends AbstractTableModel {
        private String[] columnNames = {"#",
        								"Name",
                                        "Human/Computer",
                                        "Team",
                                        "Computer Type"
                                        };
        
        public GameTableModel(Object[][] data) {
			this.data = data;
		}
        private Object[][] data;
 
        public final Object[] longValues = {"Jane", "Kathy",
                                            "None of the above",
                                            new Integer(20), Boolean.TRUE};
 
        public int getColumnCount() {
            return columnNames.length;
        }
 
        public int getRowCount() {
            return data.length;
        }
 
        public String getColumnName(int col) {
            return columnNames[col];
        }
 
        public Object getValueAt(int row, int col) {
//            System.out.println(data[row][col].getClass());
            return data[row][col];
        }
 
        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
        	return getValueAt(0, c).getClass();
        }
 

        public boolean isCellEditable(int row, int col) {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col < 1) {
                return false;
            }
            if (col == 4 && data[row][2].equals("Human"))
            	return false;
            return true;
        }
 
        /*
         * Don't need to implement this method unless your table's
         * data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col
                                   + " to " + value
                                   + " (an instance of "
                                   + value.getClass() + ")");
            }
 
            data[row][col] = value;
            fireTableCellUpdated(row, col);
 
            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }
 
        private void printDebugData() {
            int numRows = getRowCount();
            int numCols = getColumnCount();
 
            for (int i=0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j=0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }
 
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    public static void createAndShowGUI(GamePlugin gamePlugin, UIPlugin uiPlugin, String gamePluginName) {
        //Create and set up the window.
        topFrame = new JFrame(gamePluginName + " Lobby");
        
        GameLobby newContentPane = new GameLobby(gamePlugin, uiPlugin, gamePluginName);
        newContentPane.setOpaque(true); //content panes must be opaque
        topFrame.setContentPane(newContentPane);
 
        //Display the window.
        topFrame.pack();
        topFrame.setLocationRelativeTo(null);

        topFrame.setVisible(true);
    }
}
