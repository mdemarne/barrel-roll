package interfaces.view.panels;

import interfaces.view.elements.InfoElement;

public interface InfoPanel {

	public void addElement(InfoElement element);
	
}
