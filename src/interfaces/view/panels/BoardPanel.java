package interfaces.view.panels;

import interfaces.model.GameState;

public interface BoardPanel {

	void update(GameState gameState);
	
}
