package interfaces.model;
import interfaces.plugins.UIPlugin;

import java.awt.Point;
import java.awt.image.BufferedImage;

/**
 * 
 */

/**
 * This class is intended to define a Piece that would be on the board of the
 *   game. Examples of pieces include knights in chess or infantry in risk.
 *   Generally speaking the only information that is known about a piece 
 *   without knowing which game its from is which player it belongs to and its
 *   current location on the board. Any other functionality such as health of
 *   the Piece, or the specific moves its allowed to make would be reserved for
 *   an instantiation of the class.
 * 
 * @author Dev Gurjar		(dgurjar)
 * @author James Carroll	(jmcarrol)
 * @author Brad Charna 		(bcharna)
 * @author Alex Duda 		(aduda)
 * 
 */
public interface Piece {
	
	/**
	 * The player that controls the specific {@link Piece}
	 * @return The player that controls the specific {@link Piece}
	 */
	public Player getPlayer();
	
	/**
	 * The Image that corresponds to this piece.
	 * I.E the graphical representation that will appear on the board.
	 * @return the {@link BufferedImage} that represents this piece.
	 */
	public BufferedImage getImage();
	
	/**
	 * Sets the image that is to be used to represent this {@link Piece} on the
	 * 	board.
	 * @param image the new image that will represent this {@link Piece}
	 */
	public void setImage(UIPlugin uiPlugin);
	
	/**
	 * Sets the String that indicates what type of {@link Piece} this piece is
	 * @param pieceType the type of piece that you want this instance to be.
	 */
	public void setPieceType(String pieceType);
	
	
	/**
	 * Tells you what type of piece this instance is.
	 * @return the string that depicts what type of piece this instance is.
	 */
	public String getPieceType();
}
